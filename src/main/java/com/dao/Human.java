package com.dao;

import java.util.Arrays;
import java.util.Map;

public class Human {
    private String name;
    private String surname;
    private int year;
    private int iq;

    private Map schedule;

    public Human(String name, String surname, int year, int iq, Map schedule) {
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.iq = iq;

        this.schedule = schedule;
    }

    public Human(String name, String surname, int year) {
        this.name = name;
        this.surname = surname;
        this.year = year;


    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getIq() {
        return iq;
    }

    public void setIq(int iq) {
        this.iq = iq;
    }


    public Map getSchedule() {
        return schedule;
    }

    public void setSchedule(Map schedule) {
        this.schedule = schedule;
    }


    public Human() {


    }


    public String greetPet(Pet pet) {
        return "Hello, ".concat(pet.getNickname());
    }

    public String describePet(Pet pet) {
        String a = " ";
        if (pet.getTrickLevel() < 50) {
            a = "very sly";
        } else {
            a = "almost not sly";
        }

        System.out.println("I have a" + pet.getSpecies() + ", he is" + pet.getAge() + " years old, he is " + a);
        return a;
    }

    @Override
    public String toString() {
        return "Human{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", year=" + year +
                ", iq=" + iq +
                ", schedule=" + schedule +
                '}';
    }

//    public String describePet() {
//        String a = " ";
//        if (pet.getTrickLevel() < 50) {
//            a = "very sly";
//
//        } else {
//            a = "almost not sly";
//        }
//        System.out.println("I have a" + pet.getSpecies() + ", he is" + pet.getAge() + " years old, he is " + a);
//        return a;
//    }


}
