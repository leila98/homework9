package com.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class CollectionFamilyDao implements FamilyDao {


    private final List<Family> families = new ArrayList();


    @Override
    public List<Family> getAllFamilies() {
        return this.families;
    }

    @Override
    public Family getFamilyByIndex(int index) {
        return this.families.get(index);
    }

    @Override
    public void deleteFamily(int index) {
        this.families.remove(index);

    }

    @Override
    public void deleteFamily(Family family) {

        this.families.remove(family);
    }


    @Override
    public void saveFamily(Family family) {
        this.families.add(family);

    }


    public void displayAllFamilies() {
        for (Family family : this.families) {
            System.out.println(family);
        }
    }

    public void getFamiliesBiggerThan() {
        Family max = this.families.get(1);
        int membersMax = this.families.get(1).getChildren().size() + 2;

        for (Family family : this.families) {
            Family min = family;
            int membersMin = family.getChildren().size() + 2;

            if (membersMin > membersMax) {
                max = min;
            }


        }
        System.out.println("Our bigger family is" + max + "index is:");


    }


    public void getFamiliesLessThan() {

        Family min = this.families.get(1);
        int membersMin = this.families.get(1).getChildren().size() + 2;

        for (Family family : this.families) {
            Family max = family;
            int membersMax = family.getChildren().size() + 2;

            if (membersMax < membersMin) {
                min = max;
            }


        }
        System.out.println("Our less family is" + min + "index is:");

    }

    public void countFamiliesWithMemberNumber() {

        int countOfFamilies = this.families.size();

        for (Family family : this.families) {
            int countOfMembers = family.getChildren().size() + 2;
            System.out.println("countOfMembers for each Family:" + countOfMembers);

        }
        System.out.println("countOfFamilies for Family List:" + countOfFamilies);

    }

    public int count(Family family) {
        int countOfMembers = family.getChildren().size() + 2;
        return countOfMembers;
    }

    public void createNewFamily(Human human1, Human human2) {
        Family family = new Family(human1, human2);
        this.families.add(family);


    }


    public Family adoptChild(Family family, Human human2) {
        List<Human> children = family.getChildren();
        children.add(human2);

        return family;
    }


    public Family getFamilyById(int id) {
        return this.families.get(id);

    }

    public Set<Pet> getPets(int id) {

        return this.families.get(id).getPet();
    }

    public void addPet(int id, Pet pet) {
        Family family = this.families.get(id);
        Set<Pet> petSet = family.getPet();
        petSet.add(pet);
    }

    public void deleteFamilyByIndex(int index) {
        this.families.remove(index);
    }


    public void deleteAllChildrenOlderThen(int age) {

    }


}
