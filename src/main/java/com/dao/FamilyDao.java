package com.dao;

import java.util.List;
import java.util.Set;

public interface FamilyDao {
    List<Family> getAllFamilies();

    Family getFamilyByIndex(int index);

    void deleteFamily(int index);

    void deleteFamily(Family family);

    void saveFamily(Family family);


//    void displayAllFamilies ();
//    void getFamiliesBiggerThan  ();
//    void getFamiliesLessThan  ();
//void   countFamiliesWithMemberNumber  ();
//int   count  (Family family);
//  void   createNewFamily   (Human human1,Human human2);
//        Family adoptChild    (Family family,Human human2);
//        Family getFamilyById     (int id);
//        Set<Pet> getPets      (int id);
//        void addPet       (int id,Pet pet);
//    void deleteFamilyByIndex   (int index);
//
//    void deleteAllChildrenOlderThen     (int age);


}
