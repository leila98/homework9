package com.dao;

import java.util.Map;

public class Woman extends Human {
    public Woman(String name, String surname, int year, int iq, Map schedule) {
        super(name, surname, year, iq, schedule);
    }

    public Woman(String name, String surname, int year) {
        super(name, surname, year);
    }

    public Woman() {
    }

    @Override
    public String greetPet(Pet pet) {
        return super.greetPet(pet);
    }

    @Override
    public String describePet(Pet pet) {
        return super.describePet(pet);
    }


    //
    public void makeup() {
        System.out.println("She is doing her make-up");
    }
//

}
