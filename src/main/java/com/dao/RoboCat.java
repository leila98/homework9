package com.dao;

import java.util.List;
import java.util.Set;

public class RoboCat extends Pet implements Foul {
    public RoboCat(String nickname, int age, int trickLevel, Set<String> habits) {
        super(nickname, age, trickLevel, habits);
        this.species = Species.ROBOCAT;

    }

    @Override
    public void foul() {
        System.out.println("foul");
    }

    @Override
    public Species getSpecies() {
        return species;
    }

    @Override
    public void setSpecies(Species species) {
        this.species = species;
    }

    Species species;

    public RoboCat(String nickname) {
        super(nickname);
    }

    public RoboCat() {
    }

    @Override
    public void eat() {
        System.out.println("not eating");
    }

    @Override
    public void respond() {

    }
}
