package com.dao;

import java.util.List;
import java.util.Set;

public class Dog extends Pet {
    public Dog(String nickname, int age, int trickLevel, Set<String> habits) {
        super(nickname, age, trickLevel, habits);

        this.species = Species.DOG;
    }

    @Override
    public Species getSpecies() {
        return species;
    }

    @Override
    public void setSpecies(Species species) {
        this.species = species;
    }

    Species species;

    public Dog(String nickname) {
        super(nickname);
    }

    public Dog() {
    }

    @Override
    public void eat() {
        System.out.println("dog is eating");
    }

    @Override
    public void respond() {
        System.out.println("dog is responding");

    }
}
