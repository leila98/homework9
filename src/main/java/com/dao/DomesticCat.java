package com.dao;

import java.util.List;
import java.util.Set;

public class DomesticCat extends Pet {


    public DomesticCat(String nickname, int age, int trickLevel, Set<String> habits) {
        super(nickname, age, trickLevel, habits);
        this.species = Species.DOMESTICAT;
    }

    @Override
    public Species getSpecies() {
        return species;
    }

    @Override
    public void setSpecies(Species species) {
        this.species = species;
    }

    Species species;

    public DomesticCat(String nickname) {
        super(nickname);
    }

    public DomesticCat() {
    }

    @Override
    public void eat() {
        System.out.println("domesting cat eating");

    }

    @Override
    public void respond() {
        System.out.println("domesting cat responding");
    }
}
