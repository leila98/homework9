package com.service;


import com.dao.*;

import java.util.List;
import java.util.Set;

public class FamilyService {
    private final FamilyDao familyDao = new CollectionFamilyDao();


    public List<Family> getAllFamilies() {
        return familyDao.getAllFamilies();


    }

    ;

    public Family getFamilyByIndex(int index) {
        return familyDao.getFamilyByIndex(index);
    }

    ;


    public void deleteFamily(int index) {
        familyDao.deleteFamily(index);

    }


    public void deleteFamily(Family family) {
        familyDao.deleteFamily(family);
    }

    public void saveFamily(Family family) {
        familyDao.saveFamily(family);
    }


    public void displayAllFamilies() {
        for (Family family : familyDao.getAllFamilies()) {
            System.out.println(family);
        }
    }


    public Family getFamiliesBiggerThan() {
        Family max = familyDao.getFamilyByIndex(1);
        int membersMax = familyDao.getFamilyByIndex(1).getChildren().size() + 2;

        for (Family family : familyDao.getAllFamilies()) {
            Family min = family;
            int membersMin = family.getChildren().size() + 2;

            if (membersMin > membersMax) {
                max = min;
            }


        }
return max;

    }


    public Family getFamiliesLessThan() {

        Family min = familyDao.getFamilyByIndex(1);
        int membersMin = familyDao.getFamilyByIndex(1).getChildren().size() + 2;

        for (Family family : familyDao.getAllFamilies()) {
            Family max = family;
            int membersMax = family.getChildren().size() + 2;

            if (membersMax < membersMin) {
                min = max;
            }


        }
return min;
    }


    public void countFamiliesWithMemberNumber() {

        int countOfFamilies = familyDao.getAllFamilies().size();

        for (Family family : familyDao.getAllFamilies()) {
            int countOfMembers = family.getChildren().size() + 2;
            System.out.println("countOfMembers for each Family:" + countOfMembers);

        }
        System.out.println("countOfFamilies for Family List:" + countOfFamilies);

    }

    public int count(Family family) {
        int countOfMembers = family.getChildren().size() + 2;
        return countOfMembers;
    }


    public Family createNewFamily(Human human1, Human human2) {
        Family family = new Family(human1, human2);

return family;
    }

    public Family adoptChild(Family family, Human human2) {

        List<Human> children = family.getChildren();
        children.add(human2);

        return family;
    }

    public Family getFamilyById(int id) {
        return familyDao.getAllFamilies().get(id);

    }

    public Set<Pet> getPets(int id) {

        return familyDao.getAllFamilies().get(id).getPet();
    }


    public void addPet(int id, Pet pet) {
        Family family = familyDao.getAllFamilies().get(id);
        Set<Pet> petSet = family.getPet();
        petSet.add(pet);

    }

    public void deleteFamilyByIndex(int index) {
        familyDao.deleteFamily(index);
    }


    public void deleteAllChildrenOlderThen(int age) {

    }


}



