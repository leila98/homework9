package com.service;

import com.dao.*;
import org.junit.jupiter.api.Test;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;

class FamilyServiceTest {
    private FamilyService familyService = new FamilyService();
    private Object Family;

    @Test
    void getAllFamilies() {
        Human mother1 = new Human("khatira", "hasanova", 1965);
        Human father1 = new Human("arzu", "mammadov", 1964);
        Set<String> habits1 = new HashSet<String>();
        habits1.add("eat");
        habits1.add("walking");
        Dog dog = new Dog("toplan", 3, 78, habits1) {
            @Override
            public void respond() {
                System.out.println("void");
            }
        };


        RoboCat cat = new RoboCat("tomy", 34, 857, habits1) {
            @Override
            public void respond() {
                System.out.println("void");
            }
        };

        int[][] schedule1 = new int[7][1];
        Map<String, String> map1 = new HashMap<String, String>();
        map1.put(String.valueOf(DayOftheWeek.MONDAY), "Go to school");
        map1.put(String.valueOf(DayOftheWeek.TUESDAY), "Read a book");
        map1.put(String.valueOf(DayOftheWeek.WEDNESDAY), "Drink a coffee");
        map1.put(String.valueOf(DayOftheWeek.SUNDAY), "Go to school");
        map1.put(String.valueOf(DayOftheWeek.THURSDAY), "Meet with friends");
        map1.put(String.valueOf(DayOftheWeek.FRIDAY), "Buy furniture");
        map1.put(String.valueOf(DayOftheWeek.SATURDAY), "Buy the book");

        Human child1 = new Human("leila", "mammadova", 1998, 88, map1);

        Human child2 = new Human("anar", "rzayev", 2007, 43, map1);
        Human child3 = new Human("nargiz", "suleymanova", 1998, 43, map1);

        List<Human> children = new ArrayList<>();
        children.add(child1);
        children.add(child2);
        children.add(child3);
        Set<Pet> pets = new HashSet<>();
        pets.add(dog);
        pets.add(cat);


        Family family = new Family(father1, children, mother1, pets);
        Human mother2 = new Human("nazrin", "asadova", 1965);
        Human father2 = new Human("alikarimov", "mammadov", 1964);

        Fish fish = new Fish("nemo", 3, 78, habits1) {
            @Override
            public void respond() {
                System.out.println("void");
            }
        };


        DomesticCat cat2 = new DomesticCat("tomy", 34, 857, habits1) {
            @Override
            public void respond() {
                System.out.println("void");
            }
        };


        Human child4 = new Human("leila", "mammadova", 1998, 88, map1);


        List<Human> children2 = new ArrayList<>();

        children.add(child4);
        Set<Pet> pets2 = new HashSet<>();
        pets.add(cat2);
        pets.add(fish);


        Family family2 = new Family(father2, children2, mother2, pets);

        List<Family> families = familyService.getAllFamilies();


        familyService.saveFamily(family);
        familyService.saveFamily(family2);


        assertEquals(2, familyService.getAllFamilies().size());
    }

    @Test
    void getFamilyByIndex() {
        {


            Human mother1 = new Human("khatira", "hasanova", 1965);
            Human father1 = new Human("arzu", "mammadov", 1964);
            Set<String> habits1 = new HashSet<String>();
            habits1.add("eat");
            habits1.add("walking");
            Dog dog = new Dog("toplan", 3, 78, habits1) {
                @Override
                public void respond() {
                    System.out.println("void");
                }
            };


            RoboCat cat = new RoboCat("tomy", 34, 857, habits1) {
                @Override
                public void respond() {
                    System.out.println("void");
                }
            };

            int[][] schedule1 = new int[7][1];
            Map<String, String> map1 = new HashMap<String, String>();
            map1.put(String.valueOf(DayOftheWeek.MONDAY), "Go to school");
            map1.put(String.valueOf(DayOftheWeek.TUESDAY), "Read a book");
            map1.put(String.valueOf(DayOftheWeek.WEDNESDAY), "Drink a coffee");
            map1.put(String.valueOf(DayOftheWeek.SUNDAY), "Go to school");
            map1.put(String.valueOf(DayOftheWeek.THURSDAY), "Meet with friends");
            map1.put(String.valueOf(DayOftheWeek.FRIDAY), "Buy furniture");
            map1.put(String.valueOf(DayOftheWeek.SATURDAY), "Buy the book");

            Human child1 = new Human("leila", "mammadova", 1998, 88, map1);

            Human child2 = new Human("anar", "rzayev", 2007, 43, map1);
            Human child3 = new Human("nargiz", "suleymanova", 1998, 43, map1);

            List<Human> children = new ArrayList<>();
            children.add(child1);
            children.add(child2);
            children.add(child3);
            Set<Pet> pets = new HashSet<>();
            pets.add(dog);
            pets.add(cat);


            Family family = new Family(father1, children, mother1, pets);
            Human mother2 = new Human("nazrin", "asadova", 1965);
            Human father2 = new Human("alikarimov", "mammadov", 1964);

            Fish fish = new Fish("nemo", 3, 78, habits1) {
                @Override
                public void respond() {
                    System.out.println("void");
                }
            };


            DomesticCat cat2 = new DomesticCat("tomy", 34, 857, habits1) {
                @Override
                public void respond() {
                    System.out.println("void");
                }
            };


            Human child4 = new Human("leila", "mammadova", 1998, 88, map1);


            List<Human> children2 = new ArrayList<>();

            children.add(child4);
            Set<Pet> pets2 = new HashSet<>();
            pets2.add(cat2);
            pets2.add(fish);


            Family family2 = new Family(father2, children2, mother2, pets2);


            List<Family> families = familyService.getAllFamilies();


            familyService.saveFamily(family);
            familyService.saveFamily(family2);


            assertTrue(familyService.getFamilyByIndex(1) == family2);


        }
    }


    @Test
    void deleteFamily() {

        Human mother1 = new Human("khatira", "hasanova", 1965);
        Human father1 = new Human("arzu", "mammadov", 1964);
        Set<String> habits1 = new HashSet<String>();
        habits1.add("eat");
        habits1.add("walking");
        Dog dog = new Dog("toplan", 3, 78, habits1) {
            @Override
            public void respond() {
                System.out.println("void");
            }
        };


        RoboCat cat = new RoboCat("tomy", 34, 857, habits1) {
            @Override
            public void respond() {
                System.out.println("void");
            }
        };

        int[][] schedule1 = new int[7][1];
        Map<String, String> map1 = new HashMap<String, String>();
        map1.put(String.valueOf(DayOftheWeek.MONDAY), "Go to school");
        map1.put(String.valueOf(DayOftheWeek.TUESDAY), "Read a book");
        map1.put(String.valueOf(DayOftheWeek.WEDNESDAY), "Drink a coffee");
        map1.put(String.valueOf(DayOftheWeek.SUNDAY), "Go to school");
        map1.put(String.valueOf(DayOftheWeek.THURSDAY), "Meet with friends");
        map1.put(String.valueOf(DayOftheWeek.FRIDAY), "Buy furniture");
        map1.put(String.valueOf(DayOftheWeek.SATURDAY), "Buy the book");

        Human child1 = new Human("leila", "mammadova", 1998, 88, map1);

        Human child2 = new Human("anar", "rzayev", 2007, 43, map1);
        Human child3 = new Human("nargiz", "suleymanova", 1998, 43, map1);

        List<Human> children = new ArrayList<>();
        children.add(child1);
        children.add(child2);
        children.add(child3);
        Set<Pet> pets = new HashSet<>();
        pets.add(dog);
        pets.add(cat);


        Family family = new Family(father1, children, mother1, pets);
        Human mother2 = new Human("nazrin", "asadova", 1965);
        Human father2 = new Human("alikarimov", "mammadov", 1964);

        Fish fish = new Fish("nemo", 3, 78, habits1) {
            @Override
            public void respond() {
                System.out.println("void");
            }
        };


        DomesticCat cat2 = new DomesticCat("tomy", 34, 857, habits1) {
            @Override
            public void respond() {
                System.out.println("void");
            }
        };


        Human child4 = new Human("leila", "mammadova", 1998, 88, map1);


        List<Human> children2 = new ArrayList<>();

        children.add(child4);
        Set<Pet> pets2 = new HashSet<>();
        pets2.add(cat2);
        pets2.add(fish);


        Family family2 = new Family(father2, children2, mother2, pets2);


        List<Family> families = familyService.getAllFamilies();


        familyService.saveFamily(family);
        familyService.saveFamily(family2);

        int beforeDelete = families.size();
        familyService.deleteFamily(family);

        int afterDelete = families.size();

        assertTrue(beforeDelete - afterDelete == 1);


    }


    @Test
    void saveFamily() {
        Human mother1 = new Human("khatira", "hasanova", 1965);
        Human father1 = new Human("arzu", "mammadov", 1964);
        Set<String> habits1 = new HashSet<String>();
        habits1.add("eat");
        habits1.add("walking");
        Dog dog = new Dog("toplan", 3, 78, habits1) {
            @Override
            public void respond() {
                System.out.println("void");
            }
        };


        RoboCat cat = new RoboCat("tomy", 34, 857, habits1) {
            @Override
            public void respond() {
                System.out.println("void");
            }
        };

        int[][] schedule1 = new int[7][1];
        Map<String, String> map1 = new HashMap<String, String>();
        map1.put(String.valueOf(DayOftheWeek.MONDAY), "Go to school");
        map1.put(String.valueOf(DayOftheWeek.TUESDAY), "Read a book");
        map1.put(String.valueOf(DayOftheWeek.WEDNESDAY), "Drink a coffee");
        map1.put(String.valueOf(DayOftheWeek.SUNDAY), "Go to school");
        map1.put(String.valueOf(DayOftheWeek.THURSDAY), "Meet with friends");
        map1.put(String.valueOf(DayOftheWeek.FRIDAY), "Buy furniture");
        map1.put(String.valueOf(DayOftheWeek.SATURDAY), "Buy the book");

        Human child1 = new Human("leila", "mammadova", 1998, 88, map1);

        Human child2 = new Human("anar", "rzayev", 2007, 43, map1);
        Human child3 = new Human("nargiz", "suleymanova", 1998, 43, map1);

        List<Human> children = new ArrayList<>();
        children.add(child1);
        children.add(child2);
        children.add(child3);
        Set<Pet> pets = new HashSet<>();
        pets.add(dog);
        pets.add(cat);


        Family family = new Family(father1, children, mother1, pets);
        Human mother2 = new Human("nazrin", "asadova", 1965);
        Human father2 = new Human("alikarimov", "mammadov", 1964);

        Fish fish = new Fish("nemo", 3, 78, habits1) {
            @Override
            public void respond() {
                System.out.println("void");
            }
        };


        DomesticCat cat2 = new DomesticCat("tomy", 34, 857, habits1) {
            @Override
            public void respond() {
                System.out.println("void");
            }
        };


        Human child4 = new Human("leila", "mammadova", 1998, 88, map1);


        List<Human> children2 = new ArrayList<>();

        children.add(child4);
        Set<Pet> pets2 = new HashSet<>();
        pets2.add(cat2);
        pets2.add(fish);

        Family family2 = new Family(father2, children2, mother2, pets2);


        List<Family> families = familyService.getAllFamilies();

        int beforeAdd = families.size();
        familyService.saveFamily(family);
        familyService.saveFamily(family2);

        int afterAdd = families.size();


        assertTrue(beforeAdd < afterAdd);
    }


    @Test
    void count() {
        Human mother1 = new Human("khatira", "hasanova", 1965);
        Human father1 = new Human("arzu", "mammadov", 1964);
        Set<String> habits1 = new HashSet<String>();
        habits1.add("eat");
        habits1.add("walking");
        Dog dog = new Dog("toplan", 3, 78, habits1) {
            @Override
            public void respond() {
                System.out.println("void");
            }
        };


        RoboCat cat = new RoboCat("tomy", 34, 857, habits1) {
            @Override
            public void respond() {
                System.out.println("void");
            }
        };

        int[][] schedule1 = new int[7][1];
        Map<String, String> map1 = new HashMap<String, String>();
        map1.put(String.valueOf(DayOftheWeek.MONDAY), "Go to school");
        map1.put(String.valueOf(DayOftheWeek.TUESDAY), "Read a book");
        map1.put(String.valueOf(DayOftheWeek.WEDNESDAY), "Drink a coffee");
        map1.put(String.valueOf(DayOftheWeek.SUNDAY), "Go to school");
        map1.put(String.valueOf(DayOftheWeek.THURSDAY), "Meet with friends");
        map1.put(String.valueOf(DayOftheWeek.FRIDAY), "Buy furniture");
        map1.put(String.valueOf(DayOftheWeek.SATURDAY), "Buy the book");

        Human child1 = new Human("leila", "mammadova", 1998, 88, map1);

        Human child2 = new Human("anar", "rzayev", 2007, 43, map1);
        Human child3 = new Human("nargiz", "suleymanova", 1998, 43, map1);

        List<Human> children = new ArrayList<>();
        children.add(child1);
        children.add(child2);
        children.add(child3);
        Set<Pet> pets = new HashSet<>();
        pets.add(dog);
        pets.add(cat);


        Family family = new Family(father1, children, mother1, pets);
        Human mother2 = new Human("nazrin", "asadova", 1965);
        Human father2 = new Human("alikarimov", "mammadov", 1964);

        Fish fish = new Fish("nemo", 3, 78, habits1) {
            @Override
            public void respond() {
                System.out.println("void");
            }
        };


        DomesticCat cat2 = new DomesticCat("tomy", 34, 857, habits1) {
            @Override
            public void respond() {
                System.out.println("void");
            }
        };


        Human child4 = new Human("leila", "mammadova", 1998, 88, map1);


        List<Human> children2 = new ArrayList<>();

        children.add(child4);
        Set<Pet> pets2 = new HashSet<>();
        pets2.add(cat2);
        pets2.add(fish);


        Family family2 = new Family(father2, children2, mother2, pets2);


        List<Family> families = familyService.getAllFamilies();


        familyService.saveFamily(family);
        familyService.saveFamily(family2);
        assertTrue(familyService.getAllFamilies().stream().count() == 2);
    }

    @Test
    void createNewFamily() {
        Human mother1 = new Human("khatira", "hasanova", 1965);
        Human father1 = new Human("arzu", "mammadov", 1964);
        Set<String> habits1 = new HashSet<String>();
        habits1.add("eat");
        habits1.add("walking");
        Dog dog = new Dog("toplan", 3, 78, habits1) {
            @Override
            public void respond() {
                System.out.println("void");
            }
        };


        RoboCat cat = new RoboCat("tomy", 34, 857, habits1) {
            @Override
            public void respond() {
                System.out.println("void");
            }
        };

        int[][] schedule1 = new int[7][1];
        Map<String, String> map1 = new HashMap<String, String>();
        map1.put(String.valueOf(DayOftheWeek.MONDAY), "Go to school");
        map1.put(String.valueOf(DayOftheWeek.TUESDAY), "Read a book");
        map1.put(String.valueOf(DayOftheWeek.WEDNESDAY), "Drink a coffee");
        map1.put(String.valueOf(DayOftheWeek.SUNDAY), "Go to school");
        map1.put(String.valueOf(DayOftheWeek.THURSDAY), "Meet with friends");
        map1.put(String.valueOf(DayOftheWeek.FRIDAY), "Buy furniture");
        map1.put(String.valueOf(DayOftheWeek.SATURDAY), "Buy the book");

        Human child1 = new Human("leila", "mammadova", 1998, 88, map1);

        Human child2 = new Human("anar", "rzayev", 2007, 43, map1);
        Human child3 = new Human("nargiz", "suleymanova", 1998, 43, map1);

        List<Human> children = new ArrayList<>();
        children.add(child1);
        children.add(child2);
        children.add(child3);
        Set<Pet> pets = new HashSet<>();
        pets.add(dog);
        pets.add(cat);


        Family family = new Family(father1, children, mother1, pets);
        familyService.saveFamily(family);
        Human mother2 = new Human("nazrin", "asadova", 1965);
        Human father2 = new Human("alikarimov", "mammadov", 1964);

        Fish fish = new Fish("nemo", 3, 78, habits1) {
            @Override
            public void respond() {
                System.out.println("void");
            }
        };


        DomesticCat cat2 = new DomesticCat("tomy", 34, 857, habits1) {
            @Override
            public void respond() {
                System.out.println("void");
            }
        };


        Human child4 = new Human("leila", "mammadova", 1998, 88, map1);


        List<Human> children2 = new ArrayList<>();

        children.add(child4);
        Set<Pet> pets2 = new HashSet<>();
        pets2.add(cat2);
        pets2.add(fish);


        familyService.saveFamily(familyService.createNewFamily(mother2, father2));
        assertEquals(2, familyService.getAllFamilies().size());
    }

    @Test
    void adoptChild() {
        Human mother1 = new Human("khatira", "hasanova", 1965);
        Human father1 = new Human("arzu", "mammadov", 1964);
        Set<String> habits1 = new HashSet<String>();
        habits1.add("eat");
        habits1.add("walking");
        Dog dog = new Dog("toplan", 3, 78, habits1) {
            @Override
            public void respond() {
                System.out.println("void");
            }
        };


        RoboCat cat = new RoboCat("tomy", 34, 857, habits1) {
            @Override
            public void respond() {
                System.out.println("void");
            }
        };

        int[][] schedule1 = new int[7][1];
        Map<String, String> map1 = new HashMap<String, String>();
        map1.put(String.valueOf(DayOftheWeek.MONDAY), "Go to school");
        map1.put(String.valueOf(DayOftheWeek.TUESDAY), "Read a book");
        map1.put(String.valueOf(DayOftheWeek.WEDNESDAY), "Drink a coffee");
        map1.put(String.valueOf(DayOftheWeek.SUNDAY), "Go to school");
        map1.put(String.valueOf(DayOftheWeek.THURSDAY), "Meet with friends");
        map1.put(String.valueOf(DayOftheWeek.FRIDAY), "Buy furniture");
        map1.put(String.valueOf(DayOftheWeek.SATURDAY), "Buy the book");

        Human child1 = new Human("leila", "mammadova", 1998, 88, map1);

        Human child2 = new Human("anar", "rzayev", 2007, 43, map1);
        Human child3 = new Human("nargiz", "suleymanova", 1998, 43, map1);

        List<Human> children = new ArrayList<>();


        children.add(child3);
        Set<Pet> pets = new HashSet<>();
        pets.add(dog);
        pets.add(cat);

        children.add(child1);

        Family family = new Family(father1, children, mother1, pets);
        List<Family> familyLsit = familyService.getAllFamilies();
        familyService.saveFamily(family);
        int beforeAdopt = family.getChildren().size();

        family = familyService.adoptChild(family, child2);
        int afterAdopt = family.getChildren().size();

        assertEquals(afterAdopt - beforeAdopt, 1);

    }

    @Test
    void getFamilyById() {
        Human mother1 = new Human("khatira", "hasanova", 1965);
        Human father1 = new Human("arzu", "mammadov", 1964);
        Set<String> habits1 = new HashSet<String>();
        habits1.add("eat");
        habits1.add("walking");
        Dog dog = new Dog("toplan", 3, 78, habits1) {
            @Override
            public void respond() {
                System.out.println("void");
            }
        };


        RoboCat cat = new RoboCat("tomy", 34, 857, habits1) {
            @Override
            public void respond() {
                System.out.println("void");
            }
        };

        int[][] schedule1 = new int[7][1];
        Map<String, String> map1 = new HashMap<String, String>();
        map1.put(String.valueOf(DayOftheWeek.MONDAY), "Go to school");
        map1.put(String.valueOf(DayOftheWeek.TUESDAY), "Read a book");
        map1.put(String.valueOf(DayOftheWeek.WEDNESDAY), "Drink a coffee");
        map1.put(String.valueOf(DayOftheWeek.SUNDAY), "Go to school");
        map1.put(String.valueOf(DayOftheWeek.THURSDAY), "Meet with friends");
        map1.put(String.valueOf(DayOftheWeek.FRIDAY), "Buy furniture");
        map1.put(String.valueOf(DayOftheWeek.SATURDAY), "Buy the book");

        Human child1 = new Human("leila", "mammadova", 1998, 88, map1);

        Human child2 = new Human("anar", "rzayev", 2007, 43, map1);
        Human child3 = new Human("nargiz", "suleymanova", 1998, 43, map1);

        List<Human> children = new ArrayList<>();
        children.add(child1);
        children.add(child2);
        children.add(child3);
        Set<Pet> pets = new HashSet<>();
        pets.add(dog);
        pets.add(cat);


        Family family = new Family(father1, children, mother1, pets);
        Human mother2 = new Human("nazrin", "asadova", 1965);
        Human father2 = new Human("alikarimov", "mammadov", 1964);

        Fish fish = new Fish("nemo", 3, 78, habits1) {
            @Override
            public void respond() {
                System.out.println("void");
            }
        };


        DomesticCat cat2 = new DomesticCat("tomy", 34, 857, habits1) {
            @Override
            public void respond() {
                System.out.println("void");
            }
        };


        Human child4 = new Human("leila", "mammadova", 1998, 88, map1);


        List<Human> children2 = new ArrayList<>();

        children.add(child4);
        Set<Pet> pets2 = new HashSet<>();
        pets2.add(cat2);
        pets2.add(fish);


        Family family2 = new Family(father2, children2, mother2, pets2);


        List<Family> families = familyService.getAllFamilies();

        familyService.saveFamily(family);
        familyService.saveFamily(family2);
        assertEquals(familyService.getFamilyById(1), family2);
    }

    @Test
    void getPets() {
        Human mother1 = new Human("khatira", "hasanova", 1965);
        Human father1 = new Human("arzu", "mammadov", 1964);
        Set<String> habits1 = new HashSet<String>();
        habits1.add("eat");
        habits1.add("walking");
        Dog dog = new Dog("toplan", 3, 78, habits1) {
            @Override
            public void respond() {
                System.out.println("void");
            }
        };


        RoboCat cat = new RoboCat("Mastan", 34, 857, habits1) {
            @Override
            public void respond() {
                System.out.println("void");
            }
        };

        int[][] schedule1 = new int[7][1];
        Map<String, String> map1 = new HashMap<String, String>();
        map1.put(String.valueOf(DayOftheWeek.MONDAY), "Go to school");
        map1.put(String.valueOf(DayOftheWeek.TUESDAY), "Read a book");
        map1.put(String.valueOf(DayOftheWeek.WEDNESDAY), "Drink a coffee");
        map1.put(String.valueOf(DayOftheWeek.SUNDAY), "Go to school");
        map1.put(String.valueOf(DayOftheWeek.THURSDAY), "Meet with friends");
        map1.put(String.valueOf(DayOftheWeek.FRIDAY), "Buy furniture");
        map1.put(String.valueOf(DayOftheWeek.SATURDAY), "Buy the book");

        Human child1 = new Human("leila", "mammadova", 1998, 88, map1);

        Human child2 = new Human("anar", "rzayev", 2007, 43, map1);
        Human child3 = new Human("nargiz", "suleymanova", 1998, 43, map1);

        List<Human> children = new ArrayList<>();
        children.add(child1);
        children.add(child2);
        children.add(child3);
        Set<Pet> pets = new HashSet<>();
        pets.add(dog);
        pets.add(cat);


        Family family = new Family(father1, children, mother1, pets);


        List<Family> families = familyService.getAllFamilies();
        families.add(family);

        familyService.saveFamily(family);
        assertTrue(familyService.getPets(0).size() == 2);

    }

    @Test
    void addPet() {

        Human mother1 = new Human("khatira", "hasanova", 1965);
        Human father1 = new Human("arzu", "mammadov", 1964);
        Set<String> habits1 = new HashSet<String>();
        habits1.add("eat");
        habits1.add("walking");
        Dog dog = new Dog("toplan", 3, 78, habits1) {
            @Override
            public void respond() {
                System.out.println("void");
            }
        };


        DomesticCat cat = new DomesticCat("tomy", 34, 857, habits1) {
            @Override
            public void respond() {
                System.out.println("void");
            }
        };

        int[][] schedule1 = new int[7][1];
        Map<String, String> map1 = new HashMap<String, String>();
        map1.put(String.valueOf(DayOftheWeek.MONDAY), "Go to school");
        map1.put(String.valueOf(DayOftheWeek.TUESDAY), "Read a book");
        map1.put(String.valueOf(DayOftheWeek.WEDNESDAY), "Drink a coffee");
        map1.put(String.valueOf(DayOftheWeek.SUNDAY), "Go to school");
        map1.put(String.valueOf(DayOftheWeek.THURSDAY), "Meet with friends");
        map1.put(String.valueOf(DayOftheWeek.FRIDAY), "Buy furniture");
        map1.put(String.valueOf(DayOftheWeek.SATURDAY), "Buy the book");

        Human child1 = new Human("leila", "mammadova", 1998, 88, map1);

        Human child2 = new Human("anar", "rzayev", 2007, 43, map1);
        Human child3 = new Human("nargiz", "suleymanova", 1998, 43, map1);

        List<Human> children = new ArrayList<>();
        children.add(child1);
        children.add(child2);
        children.add(child3);
        Set<Pet> pets = new HashSet<>();


        Family family = new Family(father1, children, mother1, pets);
        familyService.saveFamily(family);


        int beforeAddPet = family.getPet().size();
        familyService.addPet(0, dog);

        int afterAddPet = family.getPet().size();


        assertTrue(afterAddPet - beforeAddPet == 1);
    }

    @Test
    void deleteFamilyByIndex() {
        Human mother1 = new Human("khatira", "hasanova", 1965);
        Human father1 = new Human("arzu", "mammadov", 1964);
        Set<String> habits1 = new HashSet<String>();
        habits1.add("eat");
        habits1.add("walking");
        Dog dog = new Dog("toplan", 3, 78, habits1) {
            @Override
            public void respond() {
                System.out.println("void");
            }
        };


        RoboCat cat = new RoboCat("tomy", 34, 857, habits1) {
            @Override
            public void respond() {
                System.out.println("void");
            }
        };

        int[][] schedule1 = new int[7][1];
        Map<String, String> map1 = new HashMap<String, String>();
        map1.put(String.valueOf(DayOftheWeek.MONDAY), "Go to school");
        map1.put(String.valueOf(DayOftheWeek.TUESDAY), "Read a book");
        map1.put(String.valueOf(DayOftheWeek.WEDNESDAY), "Drink a coffee");
        map1.put(String.valueOf(DayOftheWeek.SUNDAY), "Go to school");
        map1.put(String.valueOf(DayOftheWeek.THURSDAY), "Meet with friends");
        map1.put(String.valueOf(DayOftheWeek.FRIDAY), "Buy furniture");
        map1.put(String.valueOf(DayOftheWeek.SATURDAY), "Buy the book");

        Human child1 = new Human("leila", "mammadova", 1998, 88, map1);

        Human child2 = new Human("anar", "rzayev", 2007, 43, map1);
        Human child3 = new Human("nargiz", "suleymanova", 1998, 43, map1);

        List<Human> children = new ArrayList<>();
        children.add(child1);
        children.add(child2);
        children.add(child3);
        Set<Pet> pets = new HashSet<>();
        pets.add(dog);
        pets.add(cat);


        Family family = new Family(father1, children, mother1, pets);
        Human mother2 = new Human("nazrin", "asadova", 1965);
        Human father2 = new Human("alikarimov", "mammadov", 1964);

        Fish fish = new Fish("nemo", 3, 78, habits1) {
            @Override
            public void respond() {
                System.out.println("void");
            }
        };


        DomesticCat cat2 = new DomesticCat("tomy", 34, 857, habits1) {
            @Override
            public void respond() {
                System.out.println("void");
            }
        };


        Human child4 = new Human("leila", "mammadova", 1998, 88, map1);


        List<Human> children2 = new ArrayList<>();

        children.add(child4);
        Set<Pet> pets2 = new HashSet<>();
        pets2.add(cat2);
        pets2.add(fish);


        Family family2 = new Family(father2, children2, mother2, pets2);


        List<Family> families = familyService.getAllFamilies();


        familyService.saveFamily(family);
        familyService.saveFamily(family2);

        int beforeDelete = families.size();
        familyService.deleteFamilyByIndex(1);

        int afterDelete = families.size();

        assertTrue(beforeDelete - afterDelete == 1);
    }

    @Test
    void getFamiliesLessThan() {
        Human mother1 = new Human("khatira", "hasanova", 1965);
        Human father1 = new Human("arzu", "mammadov", 1964);
        Set<String> habits1 = new HashSet<String>();
        habits1.add("eat");
        habits1.add("walking");
        Dog dog = new Dog("toplan", 3, 78, habits1) {
            @Override
            public void respond() {
                System.out.println("void");
            }
        };


        RoboCat cat = new RoboCat("tomy", 34, 857, habits1) {
            @Override
            public void respond() {
                System.out.println("void");
            }
        };

        int[][] schedule1 = new int[7][1];
        Map<String, String> map1 = new HashMap<String, String>();
        map1.put(String.valueOf(DayOftheWeek.MONDAY), "Go to school");
        map1.put(String.valueOf(DayOftheWeek.TUESDAY), "Read a book");
        map1.put(String.valueOf(DayOftheWeek.WEDNESDAY), "Drink a coffee");
        map1.put(String.valueOf(DayOftheWeek.SUNDAY), "Go to school");
        map1.put(String.valueOf(DayOftheWeek.THURSDAY), "Meet with friends");
        map1.put(String.valueOf(DayOftheWeek.FRIDAY), "Buy furniture");
        map1.put(String.valueOf(DayOftheWeek.SATURDAY), "Buy the book");

        Human child1 = new Human("leila", "mammadova", 1998, 88, map1);

        Human child2 = new Human("anar", "rzayev", 2007, 43, map1);
        Human child3 = new Human("nargiz", "suleymanova", 1998, 43, map1);

        List<Human> children = new ArrayList<>();
        children.add(child1);

        Set<Pet> pets = new HashSet<>();
        pets.add(dog);
        pets.add(cat);


        Family family = new Family(father1, children, mother1, pets);
        Human mother2 = new Human("nazrin", "asadova", 1965);
        Human father2 = new Human("alikarimov", "mammadov", 1964);

        Fish fish = new Fish("nemo", 3, 78, habits1) {
            @Override
            public void respond() {
                System.out.println("void");
            }
        };


        DomesticCat cat2 = new DomesticCat("tomy", 34, 857, habits1) {
            @Override
            public void respond() {
                System.out.println("void");
            }
        };


        Human child4 = new Human("leila", "mammadova", 1998, 88, map1);


        List<Human> children2 = new ArrayList<>();

        children.add(child4);
        Set<Pet> pets2 = new HashSet<>();
        pets.add(cat2);
        pets.add(fish);


        Family family2 = new Family(father2, children2, mother2, pets);

        List<Family> families = familyService.getAllFamilies();


        familyService.saveFamily(family);
        familyService.saveFamily(family2);
        familyService.adoptChild(family2, child3);
        familyService.adoptChild(family2, child4);
        familyService.adoptChild(family2, child2);


        assertTrue(familyService.getFamiliesLessThan() == family);

    }

    @Test
    void getFamiliesBiggerThan() {
        Human mother1 = new Human("khatira", "hasanova", 1965);
        Human father1 = new Human("arzu", "mammadov", 1964);
        Set<String> habits1 = new HashSet<String>();
        habits1.add("eat");
        habits1.add("walking");
        Dog dog = new Dog("toplan", 3, 78, habits1) {
            @Override
            public void respond() {
                System.out.println("void");
            }
        };


        RoboCat cat = new RoboCat("tomy", 34, 857, habits1) {
            @Override
            public void respond() {
                System.out.println("void");
            }
        };

        int[][] schedule1 = new int[7][1];
        Map<String, String> map1 = new HashMap<String, String>();
        map1.put(String.valueOf(DayOftheWeek.MONDAY), "Go to school");
        map1.put(String.valueOf(DayOftheWeek.TUESDAY), "Read a book");
        map1.put(String.valueOf(DayOftheWeek.WEDNESDAY), "Drink a coffee");
        map1.put(String.valueOf(DayOftheWeek.SUNDAY), "Go to school");
        map1.put(String.valueOf(DayOftheWeek.THURSDAY), "Meet with friends");
        map1.put(String.valueOf(DayOftheWeek.FRIDAY), "Buy furniture");
        map1.put(String.valueOf(DayOftheWeek.SATURDAY), "Buy the book");

        Human child1 = new Human("leila", "mammadova", 1998, 88, map1);

        Human child2 = new Human("anar", "rzayev", 2007, 43, map1);
        Human child3 = new Human("nargiz", "suleymanova", 1998, 43, map1);

        List<Human> children = new ArrayList<>();
        children.add(child1);

        Set<Pet> pets = new HashSet<>();
        pets.add(dog);
        pets.add(cat);


        Family family = new Family(father1, children, mother1, pets);
        Human mother2 = new Human("nazrin", "asadova", 1965);
        Human father2 = new Human("alikarimov", "mammadov", 1964);

        Fish fish = new Fish("nemo", 3, 78, habits1) {
            @Override
            public void respond() {
                System.out.println("void");
            }
        };


        DomesticCat cat2 = new DomesticCat("tomy", 34, 857, habits1) {
            @Override
            public void respond() {
                System.out.println("void");
            }
        };


        Human child4 = new Human("leila", "mammadova", 1998, 88, map1);


        List<Human> children2 = new ArrayList<>();

        children.add(child4);
        Set<Pet> pets2 = new HashSet<>();
        pets.add(cat2);
        pets.add(fish);


        Family family2 = new Family(father2, children2, mother2, pets);

        List<Family> families = familyService.getAllFamilies();


        familyService.saveFamily(family);
        familyService.saveFamily(family2);
        familyService.adoptChild(family2, child3);
        familyService.adoptChild(family2, child4);
        familyService.adoptChild(family2, child2);


        assertTrue(familyService.getFamiliesLessThan() == family2);

    }
}