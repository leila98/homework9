package com.controller;

import com.dao.DayOftheWeek;
import com.dao.Family;
import com.dao.Human;
import com.dao.Pet;
import org.junit.jupiter.api.Test;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;

class FamilyControllerTest {
    private FamilyController familyController = new FamilyController();

    @Test
    void getAllFamilies() {


        Human mother1 = new Human("khatira", "hasanova", 1965);
        Human father1 = new Human("arzu", "mammadov", 1964);
        Set<String> habits1 = new HashSet<String>();
        habits1.add("eat");
        habits1.add("walking");
        Pet pet1 = new Pet("toplan", 3, 78, habits1) {
            @Override
            public void respond() {
                System.out.println("void");
            }
        };


        Pet pet2 = new Pet("tomy", 34, 857, habits1) {
            @Override
            public void respond() {
                System.out.println("void");
            }
        };

        int[][] schedule1 = new int[7][1];
        Map<String, String> map1 = new HashMap<String, String>();
        map1.put(String.valueOf(DayOftheWeek.MONDAY), "Go to school");
        map1.put(String.valueOf(DayOftheWeek.TUESDAY), "Read a book");
        map1.put(String.valueOf(DayOftheWeek.WEDNESDAY), "Drink a coffee");
        map1.put(String.valueOf(DayOftheWeek.FRIDAY), "Go to school");
        map1.put(String.valueOf(DayOftheWeek.SATURDAY), "Meet with friends");
        map1.put(String.valueOf(DayOftheWeek.SUNDAY), "Buy furniture");
        map1.put(String.valueOf(DayOftheWeek.THURSDAY), "Buy the book");

        Human child1 = new Human("leila", "mammadova", 1998, 88, map1);

        Human child2 = new Human("anar", "rzayev", 2007, 43, map1);
        Human child3 = new Human("nargiz", "suleymanova", 1998, 43, map1);

        List<Human> children = new ArrayList<>();
        children.add(child1);
        children.add(child2);
        children.add(child3);
        Set<Pet> pets = new HashSet<>();
        pets.add(pet2);
        pets.add(pet1);


        Family family = new Family(father1, children, mother1, (Set<Pet>) pet1);

        List<Family> familiesBefore = familyController.getAllFamilies();
        int countbeforeAdd = familiesBefore.size();
        familyController.saveFamily(family);


        List<Family> afterAdd = familyController.getAllFamilies();
        int countAfterAdd = familyController.getAllFamilies().size();
        assertTrue(countAfterAdd - countbeforeAdd == 1);
    }


    @Test
    void getFamilyByIndex() {
    }

    @Test
    void deleteFamily() {
    }

    @Test
    void testDeleteFamily() {
    }

    @Test
    void saveFamily() {
    }

    @Test
    void displayAllFamilies() {
    }

    @Test
    void getFamiliesBiggerThan() {
    }

    @Test
    void getFamiliesLessThan() {
    }

    @Test
    void countFamiliesWithMemberNumber() {
    }

    @Test
    void count() {
    }

    @Test
    void createNewFamily() {
    }

    @Test
    void adoptChild() {
    }

    @Test
    void getFamilyById() {
    }

    @Test
    void getPets() {
    }

    @Test
    void addPet() {
    }

    @Test
    void deleteFamilyByIndex() {
    }

    @Test
    void deleteAllChildrenOlderThen() {
    }
}